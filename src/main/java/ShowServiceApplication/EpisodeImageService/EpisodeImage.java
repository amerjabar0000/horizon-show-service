package ShowServiceApplication.EpisodeImageService;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import ShowServiceApplication.EpisodeService.Episode;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.io.Serializable;
import java.util.UUID;

@Entity(name = "EpisodeImages")
@Table(name = "episode_images")
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class EpisodeImage implements Serializable {
    
    @Column(name = "episode_image_id", unique = true, insertable = false, updatable = false)
    private @Id @GeneratedValue UUID episodeImageId;

    @ManyToOne
    @JoinColumn(name = "episode_id_reference", referencedColumnName = "episode_id", nullable = true, insertable = true, updatable = true)
    @JsonBackReference
    private Episode episodeReference;
    
    @Column(name = "date_of_adding", unique = false, nullable = true, columnDefinition = "TEXT")
    private String dateOfAdding;
    
    public EpisodeImage() {};

    public EpisodeImage(Episode episodeReference, String dateOfAdding) {
        this.episodeReference = episodeReference;
        this.dateOfAdding = dateOfAdding;
    }

    public EpisodeImage(UUID episodeImageId, Episode episodeReference, String dateOfAdding) {
        this.episodeImageId = episodeImageId;
        this.episodeReference = episodeReference;
        this.dateOfAdding = dateOfAdding;
    }

    public void setEpisodeImageId(UUID episodeImageId) {
        this.episodeImageId = episodeImageId;
    }

    public void setEpisodeReference(Episode episodeReference) {
        this.episodeReference = episodeReference;
    }

    public void setDateOfAdding(String dateOfAdding) {
        this.dateOfAdding = dateOfAdding;
    }

    public UUID getEpisodeImageId() {
        return episodeImageId;
    }

    public Episode getEpisodeReference() {
        return episodeReference;
    }

    public String getDateOfAdding() {
        return dateOfAdding;
    }
    
}
