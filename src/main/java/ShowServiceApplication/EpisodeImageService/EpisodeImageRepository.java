package ShowServiceApplication.EpisodeImageService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface EpisodeImageRepository extends JpaRepository<EpisodeImage, UUID> {
    
    @Query(
        value = "SELECT * FROM episode_images WHERE episode_image_id = ?",
        nativeQuery = true
    )
    Optional<EpisodeImage> findByUUID(UUID id);
    
    @Query(
        value = "SELECT * FROM episode_images WHERE episode_id_reference = ?",
        nativeQuery = true
    )
    List<Optional<EpisodeImage>> findEpisodeImagesByEpisodeId(UUID id, Pageable page);
    
    @Query(
        value = "DELETE FROM episode_images WHERE episode_id_reference = ?",
        nativeQuery = true
    )
    @Modifying
    @Transactional
    void deleteAllImagesByEpisodeId(UUID id);
    
}

