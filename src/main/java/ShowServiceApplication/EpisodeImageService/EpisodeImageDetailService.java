package ShowServiceApplication.EpisodeImageService;

import ShowServiceApplication.ImageServiceExceptions.ImageNotCreatedException;
import ShowServiceApplication.ImageServiceExceptions.ImageNotFoundException;
import ShowServiceApplication.EpisodeService.Episode;
import ShowServiceApplication.ShowService.ShowServiceExceptions.PayloadNotAcceptedException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;

// Finished this project for now. Must be integrated with the authorization of auth-service through an
// RSA and an access token. Also, all requests from the UI to get/post files should come here first, then go
// to the file server (Node.js) in order to get/post the files with the given metadata here.

// The gateway to implement at the lastest stages must route all requests from /api/shows to this service.
// Redirections from /api/**/episodes shall be decided on later after setting up the UI.

@Service
public class EpisodeImageDetailService {
    
    private final EpisodeImageRepository episodeImageRepository;
    private final EpisodeImageValidator episodeImageValidator;
    private final EpisodeImageModelAssember episodeImageAssembler;
    
    public EpisodeImageDetailService(
        EpisodeImageRepository episodeImageRepository,
        EpisodeImageValidator episodeImageValidator,
        EpisodeImageModelAssember episodeImageAssembler
    ) {
        this.episodeImageRepository = episodeImageRepository;
        this.episodeImageValidator = episodeImageValidator;
        this.episodeImageAssembler = episodeImageAssembler;
    }
    
    public EpisodeImage createEpisodeImage(EpisodeImage image) {
        
        if ( !episodeImageValidator.validateEpisodeImage(image) )
            throw new PayloadNotAcceptedException();
            
        try {
            image = episodeImageRepository.save(image);
        } catch ( Exception e ) {
            throw new ImageNotCreatedException();
        }
        
        return image;
    }
    
    public List<EntityModel<EpisodeImage>> getEpisodeImages(
        UUID episodeId, 
        int page, 
        int numberOfElements, 
        String sortBy
    ) {
        
        List<EntityModel<EpisodeImage>> images;
        
        try {
            images = episodeImageRepository
                .findEpisodeImagesByEpisodeId(episodeId, PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                .stream()
                .map(episodeImage -> episodeImage.get())
                .map(episodeImageAssembler::toModel)
                .collect(Collectors.toList());
        } catch ( Exception e ) {
            throw new ImageNotFoundException("Images of current episode does not exist.");
        }
        
        return images;
    }
    
    public EntityModel<EpisodeImage> getEpisodeImage(UUID id) {
        
        Optional<EpisodeImage> image = episodeImageRepository.findByUUID(id);        
        if ( !image.isPresent() )
            throw new ImageNotFoundException();
        
        return EntityModel.of(image.get());
    }

    public List<EntityModel<EpisodeImage>> createEpisodeImages(List<EpisodeImage> images, Episode episode) {
        
        if ( !episodeImageValidator.validateEpisodeImages(images) ) 
            throw new PayloadNotAcceptedException();
        
        
        List<EpisodeImage> newImages = new ArrayList<EpisodeImage>();
        List<EntityModel<EpisodeImage>> newImageEntities = new ArrayList();
        
        try {
            for ( int x = 0; x < images.size(); x++ ) {
                newImages.add(
                    episodeImageRepository.save(new EpisodeImage(
                        episode,
                        images.get(x).getDateOfAdding()
                    ))
                );
            }
            
            newImageEntities = newImages
                .stream()
                .map(episodeImageAssembler::toModel)
                .collect(Collectors.toList());
            
        } catch ( Exception e ) {
            throw new ImageNotCreatedException();
        }
        
        return newImageEntities;
    }
    
    public void deleteAllImagesFromEpisode(UUID episodeId) {
        try {
            episodeImageRepository.deleteAllImagesByEpisodeId(episodeId);
        } catch ( NoSuchElementException | EmptyResultDataAccessException e ) {
            throw new ImageNotFoundException();
        }
    }
    
    public void deleteEpisodeImage(UUID episodeImageId) {
        try {
            episodeImageRepository.deleteById(episodeImageId);
        } catch ( NoSuchElementException | EmptyResultDataAccessException e ) {
            throw new ImageNotFoundException();
        }
    }
    
}


