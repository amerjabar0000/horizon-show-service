package ShowServiceApplication.EpisodeImageService;

import ShowServiceApplication.ShowService.ShowController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.hateoas.server.reactive.WebFluxLinkBuilder;
import org.springframework.stereotype.Component;


@Component
public class EpisodeImageModelAssember implements RepresentationModelAssembler<EpisodeImage, EntityModel<EpisodeImage>> {
    
    @Override
    public EntityModel<EpisodeImage> toModel(EpisodeImage episode) {
        return EntityModel.of(
            episode,
            WebMvcLinkBuilder.linkTo(WebFluxLinkBuilder.methodOn(ShowController.class)
            .getEpisodeImage(
                episode.getEpisodeReference().getShowReference().getShowId(),
                episode.getEpisodeReference().getEpisodeId(),
                episode.getEpisodeImageId()
            )).withSelfRel(),
            WebMvcLinkBuilder.linkTo(WebFluxLinkBuilder.methodOn(ShowController.class)
            .getEpisodeImages(
                episode.getEpisodeReference().getShowReference().getShowId(),
                episode.getEpisodeReference().getEpisodeId(),
                0,
                0,
                null
            )).withRel("episodeImages")
        );
    }
    
}

