package ShowServiceApplication.EpisodeImageService;

import ShowServiceApplication.ShowService.ShowServiceExceptions.PayloadNotAcceptedException;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class EpisodeImageValidator {
    
    private final EpisodeImageRepository episodeImageRepository;
    
    public EpisodeImageValidator(EpisodeImageRepository episodeImageRepository) {
        this.episodeImageRepository = episodeImageRepository;
    }
    
    public boolean validateEpisodeImage(EpisodeImage image) {
        if ( image.getDateOfAdding() != null && image.getDateOfAdding().length() > 5 )
            return true;
        else
            throw new PayloadNotAcceptedException("Episode image date of adding was not valid");
    }
    
    public boolean episodeImageExists(EpisodeImage image) {
        return episodeImageRepository.findByUUID(image.getEpisodeImageId()).isPresent();
    }
    
    public boolean showImageExists(List<EpisodeImage> images) {
        for ( EpisodeImage image : images ) {
            if ( !episodeImageExists(image) )
                return false;
        }
        
        return true;
    }
    
    public boolean validateEpisodeImages(List<EpisodeImage> images) {
        
        if ( images.size() < 1 )
            return false;
        
        boolean finalState = true;
        
        for ( int x = 0; x < images.size(); x++ ) {
            if ( !validateEpisodeImage(images.get(x)) )
                finalState = false;
        }
        
        return finalState;
    }
    
}
