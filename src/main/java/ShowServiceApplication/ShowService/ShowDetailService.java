package ShowServiceApplication.ShowService;

import ShowServiceApplication.ImageServiceExceptions.ImageNotFoundException;
import ShowServiceApplication.ShowImageService.ShowImageDetailService;
import ShowServiceApplication.ShowService.ShowServiceExceptions.BadSearchQueryException;
import ShowServiceApplication.ShowService.ShowServiceExceptions.PayloadNotAcceptedException;
import ShowServiceApplication.ShowService.ShowServiceExceptions.ShowAlreadyExistsException;
import ShowServiceApplication.ShowService.ShowServiceExceptions.ShowNotFoundException;
import ShowServiceApplication.ShowService.ShowServiceExceptions.ShowNotUpdatedException;
import ShowServiceApplication.ShowImageService.ShowImage;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;

@Service
public class ShowDetailService {
    
    private final ShowRepository showRepository;
    private final ShowModelAssembler showAssembler;
    private final ShowValidator showValidator;
    private final ShowImageDetailService showImageDetailService;

    public ShowDetailService (
        ShowRepository showRepository,
        ShowModelAssembler showAssembler,
        ShowValidator showValidator,
        ShowImageDetailService showImageDetailService
    ) {
        this.showRepository = showRepository;
        this.showAssembler = showAssembler;
        this.showValidator = showValidator;
        this.showImageDetailService = showImageDetailService;
    }
    
    public List<EntityModel<Show>> getShows(
        String name, 
        String director, 
        Double rating, 
        String genre, 
        String language, 
        Integer popularity,
        Integer yearOfRelease,
        boolean multipleQueries,
        Integer page, 
        Integer numberOfElements, 
        String sortBy
    ) {
        
        List<EntityModel<Show>> shows;
        
        System.out.println("mul:" + multipleQueries + "\t" + rating + ", " + genre + ", " + language + ", " + popularity + ", " + yearOfRelease);
        
        try {            
            if ( multipleQueries ) {
                shows = showRepository
                    .findAllShowsByAllFilters(
                            rating == null ? 0 : rating.doubleValue(), 
                            genre == null ? "" : genre, 
                            language == null ? "" : language, 
                            popularity == null ? 0 : popularity.intValue(), 
                            yearOfRelease == null ? 0 : yearOfRelease.intValue(), 
                            PageRequest.of(page, numberOfElements, Sort.by(sortBy).descending())
                    )
                    .stream()
                    .map(showAssembler::toModel)
                    .collect(Collectors.toList());
            } else {            
                if ( name != null ) {
                    shows = showRepository
                        .findAllByName(name, PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                        .stream()
                        .map(showAssembler::toModel)
                        .collect(Collectors.toList());
                } else if ( director != null ) {
                    shows = showRepository.findAllByDirector(director, PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                        .stream()
                        .map(showAssembler::toModel)
                        .collect(Collectors.toList());
                } else if ( rating != null && rating >= 1 ) {
                    shows = showRepository.findAllByRating(rating, PageRequest.of(page, numberOfElements, Sort.by(sortBy).descending()))
                        .stream()
                        .map(showAssembler::toModel)
                        .collect(Collectors.toList());
                } else if ( genre != null ) {
                    shows = showRepository.findAllByGenre(genre, PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                        .stream()
                        .map(showAssembler::toModel)
                        .collect(Collectors.toList());
                } else if ( language != null ) {
                    shows = showRepository.findAllByLanguage(language, PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                        .stream()
                        .map(showAssembler::toModel)
                        .collect(Collectors.toList());
                } else if ( popularity != null && popularity >= 1 ) {
                    shows = showRepository.findAllByPopularity(popularity, PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                        .stream()
                        .map(showAssembler::toModel)
                        .collect(Collectors.toList());
                } else if ( yearOfRelease != null && yearOfRelease > 1900 ) {
                    shows = showRepository.findAllByYearOfRelease(yearOfRelease, PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                        .stream()
                        .map(showAssembler::toModel)
                        .collect(Collectors.toList());
                } else {
                    shows = showRepository
                        .findAll()
                        .stream()
                        .map(showAssembler::toModel)
                        .collect(Collectors.toList());
                }
            }


        } catch ( Exception e ) {
            System.err.println(e);
            throw new BadSearchQueryException();
        }
        return shows;
    }
    
    public EntityModel<Show> getShowById(UUID id) {
        
        Optional<Show> show = showRepository.findById(id);
        
        if ( !show.isPresent() )
            throw new ShowNotFoundException(id);
            
        return showAssembler.toModel(show.get());
    }
    
    public EntityModel<Show> createShow(Show newShow) {
        
        if ( !showValidator.validateShow(newShow) )
            throw new PayloadNotAcceptedException();
        
        if ( showRepository.findOneByName(newShow.getName()).isPresent() )
            throw new ShowAlreadyExistsException();
        
        showRepository.save(newShow);
        Optional<Show> show = showRepository.findOneByName(newShow.getName());
        
        return EntityModel.of(show.get());
    }
    
    public EntityModel<Show> updateShow(UUID showId, Show newShow) {
        
        if ( !showValidator.validateShowForUpdate(newShow) )
            throw new PayloadNotAcceptedException();

        Optional<Show> show = showRepository.findById(newShow.getShowId());
        
        if ( !show.isPresent() || !newShow.getShowId().equals(showId) )
            throw new ShowNotFoundException(newShow.getShowId());
        
        // The put request made here must have in episodes and images at least
        // empty arrays []. Sending nothing as a value would cause the properties
        // to become null. Therefore a beautiful internal server error.
        try {
            newShow.setImages(show.get().getImages());
            newShow.setEpisodes(show.get().getEpisodes());
            showRepository.save(newShow);
        } catch ( Exception e ) {
            throw new ShowNotUpdatedException(showId);
        }
        
        return EntityModel.of(newShow);
    }
    
    public EntityModel<Show> deleteShow(UUID id) {

        Optional<Show> show;
        
        try {
            // There are many ways to automatically delete children entities after deletion of the parent.
            // The one we follow is by using the native deleteById along with cascade.All.
            // The previous way was using the orphanRemoval technique by removing entities from the list
            // that contained the children of the parent. Hence, orphans were deleted.
            // NOTE: The error of key constraints violation can occur if you use your custom delete query.
            // That way, the deletes for the children entities will not be called and they will not be deleted 
            // even if they still have a foreign key.
            
            show = showRepository.findByUUID(id);
            showRepository.deleteById(id);
            
        } catch ( EmptyResultDataAccessException | NoSuchElementException e ) {
            throw new ShowNotFoundException("delete", id);
        }
        
        return EntityModel.of(show.get());
    }
    
    public EntityModel<Show> deleteAllImagesOfShow(UUID id) {
        
        Optional<Show> show = showRepository.findByUUID(id);

        if ( !show.isPresent() )
            throw new ShowNotFoundException(id);
        
        try {
            showImageDetailService.deleteAllShowImages(id);
        } catch ( Exception e ) {
            throw new ShowNotUpdatedException(id);
        }
        
        return EntityModel.of(show.get());
    }
    
    public EntityModel<Show> deleteImageOfShow(UUID showId, UUID imageId) {
    
        Optional<Show> show = showRepository.findByUUID(showId);
        Optional<ShowImage> image = showImageDetailService.getShowImage(imageId);
        
        if ( !show.isPresent() )
            throw new ShowNotFoundException(showId);
        if ( !image.isPresent() )
            throw new ImageNotFoundException();

        try {
            showImageDetailService.deleteShowImage(image.get());
        } catch ( Exception e ) {
            throw new ShowNotUpdatedException(showId);
        }

        return EntityModel.of(show.get());
    }
    
    public void deleteAllExistingData() {
        showRepository.deleteAll();
    }
        
}
