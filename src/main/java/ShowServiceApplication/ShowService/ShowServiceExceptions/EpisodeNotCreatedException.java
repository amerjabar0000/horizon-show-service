package ShowServiceApplication.ShowService.ShowServiceExceptions;

public class EpisodeNotCreatedException extends RuntimeException {
    public EpisodeNotCreatedException() {
        super("Could not create episode.");
    }
    
    public EpisodeNotCreatedException(String message) {
        super(message);
    }
}


