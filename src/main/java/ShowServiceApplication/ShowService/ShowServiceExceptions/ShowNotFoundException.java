package ShowServiceApplication.ShowService.ShowServiceExceptions;

import java.util.UUID;

public class ShowNotFoundException extends RuntimeException {
    
    public ShowNotFoundException(UUID id) {
        super("Could not find show with id: " + id);
    } 
    
    public ShowNotFoundException(String action, UUID id) {
        super("Could not " + action + " show with id: " + id);
    } 
        
}
