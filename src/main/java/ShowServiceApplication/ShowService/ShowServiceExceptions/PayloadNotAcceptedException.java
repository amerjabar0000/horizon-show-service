package ShowServiceApplication.ShowService.ShowServiceExceptions;

public class PayloadNotAcceptedException extends RuntimeException {
    public PayloadNotAcceptedException() {
        super("Payload was not accepted due to missing fields!");
    }
    
    public PayloadNotAcceptedException(String message) {
        super(message);
    }
}
