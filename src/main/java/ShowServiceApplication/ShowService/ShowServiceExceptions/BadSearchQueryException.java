package ShowServiceApplication.ShowService.ShowServiceExceptions;

public class BadSearchQueryException extends RuntimeException {
    
    public BadSearchQueryException() {
        super("Bad search query could not return any results.");
    } 
}

