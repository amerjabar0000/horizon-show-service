package ShowServiceApplication.ShowService.ShowServiceExceptions;

public class ShowAlreadyExistsException extends RuntimeException {
    
    public ShowAlreadyExistsException() {
        super("Show already exists in the database.");
    } 
}

