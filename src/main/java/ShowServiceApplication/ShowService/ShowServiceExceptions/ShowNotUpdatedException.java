package ShowServiceApplication.ShowService.ShowServiceExceptions;

import java.util.UUID;

public class ShowNotUpdatedException extends RuntimeException {
    
    public ShowNotUpdatedException(UUID id) {
        super("Could not update show with id: " + id);
    } 
    
}
