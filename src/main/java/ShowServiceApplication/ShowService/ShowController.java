
package ShowServiceApplication.ShowService;

import ShowServiceApplication.EpisodeImageService.EpisodeImageDetailService;
import ShowServiceApplication.EpisodeService.Episode;
import ShowServiceApplication.EpisodeService.EpisodeDetailService;
import ShowServiceApplication.ShowImageService.ShowImage;
import ShowServiceApplication.ShowImageService.ShowImageDetailService;
import ShowServiceApplication.EpisodeImageService.EpisodeImage;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.CollectionModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@RestController
public class ShowController {
    
    private final ShowDetailService showDetailService;
    private final ShowImageDetailService showImageDetailService;
    private final EpisodeDetailService episodeDetailService;    
    private final EpisodeImageDetailService episodeImageDetailService;
    
    public ShowController(
            ShowDetailService showDetailService,
            ShowImageDetailService showImageDetailService,
            EpisodeDetailService episodeDetailService,
            EpisodeImageDetailService episodeImageDetailService,
            RabbitTemplate messagingTemplate
    ) {
        this.showDetailService = showDetailService;
        this.showImageDetailService = showImageDetailService;
        this.episodeDetailService = episodeDetailService;
        this.episodeImageDetailService = episodeImageDetailService;
    }
    
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedMethods("GET", "POST", "PUT", "DELETE");
            }
        };
    }
    
    // All routes related to shows.
    @GetMapping("/api/shows")
    public ResponseEntity<CollectionModel<EntityModel<Show>>> getShows(
        @RequestParam(required = false) String name,
        @RequestParam(required = false) String director, 
        @RequestParam(required = false) Double rating, 
        @RequestParam(required = false) String genre, 
        @RequestParam(required = false) String language, 
        @RequestParam(required = false) Integer popularity,
        @RequestParam(required = false) Integer yearOfRelease,
        @RequestParam(required = false) boolean multipleQueries,        
        @RequestParam(required = true) Integer page, 
        @RequestParam(required = true) Integer numberOfElements, 
        @RequestParam(required = true) String sortBy
    ) {
        
        List<EntityModel<Show>> shows = showDetailService.getShows(
            name,
            director,
            rating,
            genre,
            language,
            popularity,
            yearOfRelease,
            multipleQueries,
            page,
            numberOfElements,
            sortBy
        );
        
        return ResponseEntity
                .ok()
                .body(
                CollectionModel.of(
                    shows,
                    WebMvcLinkBuilder.linkTo(
                        WebMvcLinkBuilder
                            .methodOn(ShowController.class)
                            .getShows(null, null, null, null, null, 0, 0, false, 0, 0, null)
                    ).withSelfRel()
                ));
    }
    
    @GetMapping("/api/shows/{id}")
    public ResponseEntity<EntityModel<Show>> getShow(@PathVariable UUID id) {
        
        EntityModel<Show> show = showDetailService.getShowById(id);
        
        return ResponseEntity
                .ok()
                .body(show);
    }
    
    @PostMapping("/api/shows")
    public ResponseEntity<EntityModel<Show>> createShow(@RequestBody Show newShow) {
        
        EntityModel<Show> show = showDetailService.createShow(newShow);
        
        return ResponseEntity
                .created(URI.create("/api/shows/" + show.getContent().getShowId()))
                .body(show);
    }
    
    @PutMapping("/api/shows/{showId}")
    public ResponseEntity<EntityModel<Show>> replaceShow(
        @RequestBody Show newShow,
        @PathVariable UUID showId
    ) {
        
        EntityModel<Show> show = showDetailService.updateShow(showId, newShow);
        
        return ResponseEntity
                .ok()
                .body(show);
    }

    @DeleteMapping("/api/shows/{id}")
    public ResponseEntity<EntityModel<Show>> deleteShow(@PathVariable UUID id) {
        
        EntityModel<Show> show = showDetailService.deleteShow(id);
        
        return ResponseEntity
                .ok()
                .body(show);
    }
    
    // All routes related to show images.
    @GetMapping("/api/shows/{showId}/images")
    public ResponseEntity<List<ShowImage>> getShowImages(
        @PathVariable UUID showId,
        @RequestParam int page,
        @RequestParam int numberOfElements,
        @RequestParam String sortBy
    ) {
        
        List<ShowImage> images = showImageDetailService.getImagesOfShow(showId, page, numberOfElements, sortBy);
        
        return ResponseEntity
                .ok()
                .body(images);
    }
    
    @PostMapping("/api/shows/{showId}/images")
    public ResponseEntity<List<ShowImage>> uploadImageToShow(
        @PathVariable UUID showId,
        @RequestBody ArrayList<ShowImage> newImages
    ) {
        
        EntityModel<Show> show = showDetailService.getShowById(showId);
        List<ShowImage> images = showImageDetailService.createShowImages(newImages, show.getContent());

        // WARNING -- the output message which is the images object will be
        // a list of images, each image which has a show property that is
        // the parent of it in the JPA relationship. In JSON, because they are
        // both calling each other, it will become recursive after serialization.
        // The solution is to use @JsonManagedReference on the referenced object
        // and @JsonBackReference on the referencing object.
        return ResponseEntity
                .ok()
                .body(images);
    }
    
    @DeleteMapping("/api/shows/{showId}/images")
    public ResponseEntity<EntityModel<Show>> deleteImagesFromShow(@PathVariable UUID showId) {
        
        EntityModel<Show> show = showDetailService.deleteAllImagesOfShow(showId);
        
        return ResponseEntity
                .ok()
                .body(show);
    }
       
    @DeleteMapping("/api/shows/{showId}/images/{imageId}")
    public ResponseEntity<EntityModel<Show>> deleteImageFromShow(
        @PathVariable UUID showId,
        @PathVariable UUID imageId
    ) {
        
        EntityModel<Show> show = showDetailService.deleteImageOfShow(showId, imageId);
        
        return ResponseEntity
                .ok()
                .body(show);
    }
    
    // All routes related to episodes
    @GetMapping("/api/shows/{showId}/episodes")
    public ResponseEntity<List<EntityModel<Episode>>> getEpisodes(
        @RequestParam(required = false) String name,
        @RequestParam(required = false) Double rating,
        @RequestParam(required = false) Integer season,
        @RequestParam(required = true) Integer page,
        @RequestParam(required = true) Integer numberOfElements,
        @RequestParam(required = true) String sortBy        
    ) {

        List<EntityModel<Episode>> episodes = episodeDetailService.getEpisodes(
            name, 
            rating,
            season,
            page, 
            numberOfElements, 
            sortBy
        );
        
        return ResponseEntity
                .ok()
                .body(episodes);
    }
    
    @GetMapping("/api/shows/{showId}/episodes/{episodeId}")
    public ResponseEntity<EntityModel<Episode>> getEpisode(
        @PathVariable UUID showId,
        @PathVariable UUID episodeId
    ) {

        showDetailService.getShowById(showId);
        EntityModel<Episode> episode = episodeDetailService.getEpisode(episodeId);
        
        return ResponseEntity
                .ok()
                .body(episode);
    }
    
    @PostMapping("/api/shows/{showId}/episodes")
    public ResponseEntity<EntityModel<Episode>> createEpisode(
        @PathVariable UUID showId,
        @RequestBody Episode episode
    ) {
                
        EntityModel<Show> show = showDetailService.getShowById(showId);
        EntityModel<Episode> createdEpisode = episodeDetailService.createEpisode(show.getContent(), episode);
        
        return ResponseEntity
                .ok()
                .body(createdEpisode);
    }
    
    @PutMapping("/api/shows/{showId}/episodes/{episodeId}")
    public ResponseEntity<EntityModel<Episode>> updateEpisode(
        @PathVariable UUID showId,
        @PathVariable UUID episodeId,
        @RequestBody Episode episode
    ) {
        
        showDetailService.getShowById(showId);
        EntityModel<Episode> updatedEpisode = episodeDetailService.updateEpisode(episodeId, episode);
        
        return ResponseEntity
                .ok()
                .body(updatedEpisode);
    } 
    
    @DeleteMapping("/api/shows/{showId}/episodes/{episodeId}")
    public ResponseEntity<EntityModel<Episode>> deleteEpisode(
        @PathVariable UUID showId,
        @PathVariable UUID episodeId
    ) {
        
        showDetailService.getShowById(showId);
        EntityModel<Episode> deletedEpisode = episodeDetailService.deleteEpisode(episodeId);
        
        return ResponseEntity
                .ok()
                .body(deletedEpisode);
    }
    
    @DeleteMapping("/api/shows/{showId}/episodes")
    public ResponseEntity deleteEpisodes(@PathVariable UUID showId) {
        
        showDetailService.getShowById(showId);
        episodeDetailService.deleteEpisodes(showId);
        
        return ResponseEntity
                .ok()
                .build();
    }
        
    // All routes related to episode images.
    @GetMapping("/api/shows/{showId}/episode/{episodeId}/images/{episodeImageId}")
    public ResponseEntity<EntityModel<EpisodeImage>> getEpisodeImage(
        @PathVariable UUID showId,
        @PathVariable UUID episodeId,
        @PathVariable UUID episodeImageId
    ) {
        
        showDetailService.getShowById(showId);
        episodeDetailService.getEpisode(episodeId);
        EntityModel<EpisodeImage> episodeImage = episodeImageDetailService.getEpisodeImage(episodeImageId);
        
        return ResponseEntity
                .ok()
                .body(episodeImage);
    }
    
    @GetMapping("/api/shows/{showId}/episode/{episodeId}/images")
    public ResponseEntity<List<EntityModel<EpisodeImage>>> getEpisodeImages(
        @PathVariable UUID showId,
        @PathVariable UUID episodeId,
        @RequestParam int page,
        @RequestParam int numberOfElements,
        @RequestParam String sortBy
    ) {
        
        showDetailService.getShowById(showId);
        List<EntityModel<EpisodeImage>> images = episodeImageDetailService.getEpisodeImages(episodeId, page, numberOfElements, sortBy);
        
        return ResponseEntity
                .ok()
                .body(images);
    }
    
    @PostMapping("/api/shows/{showId}/episodes/{episodeId}/images")
    public ResponseEntity<List<EntityModel<EpisodeImage>>> uploadImageToEpisode(
        @PathVariable UUID showId,
        @PathVariable UUID episodeId,
        @RequestBody List<EpisodeImage> newImages
    ) {
        
        showDetailService.getShowById(showId);
        EntityModel<Episode> episode = episodeDetailService.getEpisode(episodeId);
        List<EntityModel<EpisodeImage>> episodeImages  = episodeImageDetailService.createEpisodeImages(newImages, episode.getContent());
        
        return ResponseEntity
                .ok()
                .body(episodeImages);
    }
    
    @DeleteMapping("/api/shows/{showId}/episodes/{episodeId}/images")
    public ResponseEntity deleteImagesFromEpisode(
        @PathVariable UUID showId,
        @PathVariable UUID episodeId
    ) {
        
        episodeImageDetailService.deleteAllImagesFromEpisode(episodeId);
        
        return ResponseEntity
                .ok()
                .build();
    }
    
    @DeleteMapping("/api/shows/{showId}/episodes/{episodeId}/images/{episodeImageId}")
    public ResponseEntity deleteImageFromEpisode(
        @PathVariable UUID showId,
        @PathVariable UUID episodeId,
        @PathVariable UUID episodeImageId
    ) {
        
        showDetailService.getShowById(showId);
        episodeDetailService.getEpisode(episodeId);
        episodeImageDetailService.deleteEpisodeImage(episodeImageId);
        
        return ResponseEntity
                .ok()
                .build();
    }
    
}