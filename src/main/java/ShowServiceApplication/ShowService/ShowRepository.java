package ShowServiceApplication.ShowService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/*
    Please listen to me before you try to mess anything up as I was doing
    for the last 12 hours. In the one-to-many relationship (as much as I know)
    mapping and population between objects happen automatically if anything
    is added that can manually be joined. To get elements joined, you
    don't have to do anything, they are joined automatically, do you hear
    what I am saying? They join automatically for all cases. Now it is time
    for another commit.
*/

@Repository
public interface ShowRepository extends JpaRepository<Show, UUID> {
    
    @Query(
        value = "SELECT * FROM shows WHERE shows.show_id = ?",
        nativeQuery = true
    )
    Optional<Show> findByUUID(UUID id);
    
    @Query(
        value = "SELECT * FROM shows",
        nativeQuery = true
    )
    List<Show> findAllShows(Pageable page);
    
    @Query(
        value = "DELETE FROM shows WHERE shows.show_id = ?",
        nativeQuery = true
    )
    @Transactional
    @Modifying
    void deleteByUUID(UUID id);
    
    @Query(
        value = "SELECT * FROM shows WHERE name ILIKE %?%",
        nativeQuery = true
    )
    Page<Show> findAllByName(String name, Pageable page);

    @Query(
        value = "SELECT * FROM shows WHERE rating > ?",
        nativeQuery = true
    )
    Page<Show> findAllByRating(double rating, Pageable page);

    @Query(
        value = "SELECT * FROM shows WHERE director = ?",
        nativeQuery = true
    )
    Page<Show> findAllByDirector(String director, Pageable page);

    @Query(
        value = "SELECT * FROM shows WHERE genre = ?",
        nativeQuery = true
    )
    Page<Show> findAllByGenre(String genre, Pageable page);

    @Query(
        value = "SELECT * FROM shows WHERE popularity > ?",
        nativeQuery = true
    )
    Page<Show> findAllByPopularity(int popularity, Pageable page);

    @Query(
        value = "SELECT * FROM shows WHERE language = ?",
        nativeQuery = true
    )
    Page<Show> findAllByLanguage(String language, Pageable page);

    @Query(
        value = "SELECT * FROM shows WHERE year_of_release = ?",
        nativeQuery = true
    )
    Page<Show> findAllByYearOfRelease(int yearOfRelease, Pageable page);    
    
    @Query(
        value = "SELECT * FROM shows WHERE name = ?",
        nativeQuery = true
    )
    Optional<Show> findOneByName(String name); 
    
    @Query(
        value = "SELECT * FROM shows WHERE rating > ? AND genre ILIKE %?% AND language ILIKE %?% AND popularity > ? AND year_of_release > ?",
        nativeQuery = true
    )
    Page<Show> findAllShowsByAllFilters(double rating, String genre, String language, int popularity, int yearOfRelease, Pageable page);    
    
}

