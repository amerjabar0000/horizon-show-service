package ShowServiceApplication.ShowService;

import ShowServiceApplication.EpisodeService.Episode;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import ShowServiceApplication.ShowImageService.ShowImage;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.io.Serializable;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;

@Entity(name = "Shows")
@Table(name = "shows")
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Show implements Serializable {

    @Column(name = "show_id", unique = true, nullable = false)
    private @Id @GeneratedValue UUID showId;
    
    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Column(name = "director", nullable = true)
    private String director;

    @Column(name = "writer", nullable = true)
    private String writer;
    
    @Column(name = "plot", nullable = true, length = 1000)
    private String plot;
    
    @Column(name = "year_of_release", nullable = true)
    private int yearOfRelease;

    @Column(name = "genre", nullable = true)
    private String genre;

    @Column(name = "certificate", nullable = true)
    private String certificate;

    @Column(name = "language", nullable = true)
    private String language;

    @Column(name = "popularity", nullable = true)
    private int popularity;

    @Column(name = "episode_number", nullable = true)
    private int episodeNumber;

    @Column(name = "seasons", nullable = true)
    private int seasons;

    @Column(name = "episode_per_season", nullable = true)
    private int[] episodePerSeason;

    @Column(name = "episode_length", nullable = true)
    private double episodeLength;

    @Column(name = "rating", nullable = false)
    private double rating;
    
    // Note: the class is supposed to be the parent (yikes) has an attribute, right?
    // whether that attribute is a single value in a one-to-one relationship or a
    // one-to-many as a list, you should always put @JsonManagedReference anotation
    // on the attribute that is inside of the parent (the child attribute). and the
    // @JsonBackReference on the parent attribute inside the cild class that has it.
    // If you do backwards, you will get empty children in JSON formats (I dont't 
    // know about other formats). This might be the 8th commit today, stay tuned.
    // Note 2: the following two properties should always be an array even if they
    // are empty. Setting them by default would make them null. Therefore, internal
    // server errors would happen on updates.
    // Note 3: mappedBy is extremely important for cascade deletion. You must either
    // not specify the mappedBy and when you delete a parent, you delete all of its
    // children first (will not delete the children automatically). Or specify 
    // mappedBy and when you delete a parent, cascade.REMOVE will be invoked on
    // the children entities.
    @OneToMany(mappedBy = "showReference", targetEntity = ShowImage.class, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<ShowImage> images;
    
    @OneToMany(mappedBy = "showReference", targetEntity = Episode.class, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<Episode> episodes; 
   
    public Show() {}

    public Show(UUID showId, String name, String director, String writer, String plot, int yearOfRelease, String genre, String certificate, String language, int popularity, int episodeNumber, int seasons, int[] episodePerSeason, double episodeLength, double rating, List<ShowImage> images, List<Episode> episodes) {
        this.showId = showId;
        this.name = name;
        this.director = director;
        this.writer = writer;
        this.plot = plot;
        this.yearOfRelease = yearOfRelease;
        this.genre = genre;
        this.certificate = certificate;
        this.language = language;
        this.popularity = popularity;
        this.episodeNumber = episodeNumber;
        this.seasons = seasons;
        this.episodePerSeason = episodePerSeason;
        this.episodeLength = episodeLength;
        this.rating = rating;
        this.images = images;
        this.episodes = episodes;
    }
    
    public void addImage(List<ShowImage> images) {
        images.forEach(image -> this.images.add(image));
    }
    
    public void removeImage(UUID imageId) {
        for ( int x = 0; x < this.images.size(); x++ ) {
            if ( this.images.get(x).getShowImageId().equals(imageId) )
                this.images.remove(x);
        }
    }
    
    public void removeAllImages() {
        while ( !this.images.isEmpty() )
            this.images.remove(0);
    }
    
    public void addEpisode(List<Episode> episodes) {
        episodes.forEach(episode -> this.episodes.add(episode));
    }
    
    public void removeEpisode(UUID episodeId) {
        for ( int x = 0; x < this.episodes.size(); x++ ) {
            if ( this.episodes.get(x).getEpisodeId().equals(episodeId) )
                this.episodes.remove(x);
        }
    }    
        
    public void removeAllEpisodes() {
        while ( !this.episodes.isEmpty())
            this.episodes.remove(0);
    }

    public void setShowId(UUID showId) {
        this.showId = showId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public void setSeasons(int seasons) {
        this.seasons = seasons;
    }

    public void setEpisodePerSeason(int[] episodePerSeason) {
        this.episodePerSeason = episodePerSeason;
    }

    public void setEpisodeLength(double episodeLength) {
        this.episodeLength = episodeLength;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
    
    public void setLanguage(String language) {
        this.language = language;
    }

    public void setImages(List<ShowImage> images) {
        this.images = images;
    }

    public void setEpisodes(List<Episode> episodes) {
        this.episodes = episodes;
    }

    public List<Episode> getEpisodes() {
        return episodes;
    }

    public String getLanguage() {
        return language;
    }

    public List<ShowImage> getImages() {
        return images;
    }

    public UUID getShowId() {
        return showId;
    }

    public String getName() {
        return name;
    }

    public String getDirector() {
        return director;
    }

    public String getWriter() {
        return writer;
    }

    public String getPlot() {
        return plot;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public String getGenre() {
        return genre;
    }

    public String getCertificate() {
        return certificate;
    }

    public int getPopularity() {
        return popularity;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public int getSeasons() {
        return seasons;
    }

    public int[] getEpisodePerSeason() {
        return episodePerSeason;
    }

    public double getEpisodeLength() {
        return episodeLength;
    }

    public double getRating() {
        return rating;
    }
    
}