package ShowServiceApplication.ShowService.ShowServiceResponses;

import ShowServiceApplication.ShowService.ShowServiceExceptions.ShowNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ShowNotFoundResponse {
    
    @ResponseBody
    @ExceptionHandler(ShowNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String showNotFoundHandler(ShowNotFoundException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
