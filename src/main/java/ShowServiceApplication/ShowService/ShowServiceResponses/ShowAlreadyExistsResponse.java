package ShowServiceApplication.ShowService.ShowServiceResponses;

import ShowServiceApplication.ShowService.ShowServiceExceptions.ShowAlreadyExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ShowAlreadyExistsResponse {
    
    @ResponseBody
    @ExceptionHandler(ShowAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String showAlreadyExistsHandler(ShowAlreadyExistsException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
