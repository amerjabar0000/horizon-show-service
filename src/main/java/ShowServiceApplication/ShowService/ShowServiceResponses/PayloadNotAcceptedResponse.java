package ShowServiceApplication.ShowService.ShowServiceResponses;

import ShowServiceApplication.ShowService.ShowServiceExceptions.PayloadNotAcceptedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class PayloadNotAcceptedResponse {
    
    @ResponseBody
    @ExceptionHandler(PayloadNotAcceptedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String payloadNotAcceptedHandler(PayloadNotAcceptedException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
