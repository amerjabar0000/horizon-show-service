package ShowServiceApplication.ShowService.ShowServiceResponses;

import ShowServiceApplication.ShowService.ShowServiceExceptions.ShowNotUpdatedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ShowNotUpdatedResponse {
    
    @ResponseBody
    @ExceptionHandler(ShowNotUpdatedException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String showNotUpdatedHandler(ShowNotUpdatedException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
