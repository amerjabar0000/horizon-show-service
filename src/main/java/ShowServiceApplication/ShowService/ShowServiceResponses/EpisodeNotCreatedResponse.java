package ShowServiceApplication.ShowService.ShowServiceResponses;

import ShowServiceApplication.ShowService.ShowServiceExceptions.EpisodeNotCreatedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class EpisodeNotCreatedResponse {
    
    @ResponseBody
    @ExceptionHandler(EpisodeNotCreatedException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String episodeNotCreatedHandler(EpisodeNotCreatedException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
