package ShowServiceApplication.ShowService.ShowServiceResponses;

import ShowServiceApplication.ShowService.ShowServiceExceptions.BadSearchQueryException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class BadSearchQueryResponse {
    
    @ResponseBody
    @ExceptionHandler(BadSearchQueryException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String badSearchQueryHandler(BadSearchQueryException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
