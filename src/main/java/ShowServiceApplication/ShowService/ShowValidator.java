package ShowServiceApplication.ShowService;

import ShowServiceApplication.ShowImageService.ShowImageValidator;
import ShowServiceApplication.ShowService.ShowServiceExceptions.PayloadNotAcceptedException;
import java.util.Arrays;
import org.springframework.stereotype.Component;

@Component
public class ShowValidator {
    
    ShowImageValidator imageValidator;
    
    public ShowValidator(ShowImageValidator imageValidator) {
        this.imageValidator = imageValidator;
    }
    
    public boolean validateShow(Show show) {
        boolean[] hasAllFields = new boolean[14];       
        
        if ( show.getName() != null && show.getName().length() > 5 && show.getName().length() < 40 )
            hasAllFields[0] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show name is invalid.");
        if ( show.getEpisodeLength() > 10 && show.getEpisodeLength() < 120  )
            hasAllFields[1] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show episode length is invalid.");
        if ( show.getEpisodeNumber() > 0 && show.getEpisodeNumber() < 100 )
            hasAllFields[2] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show episode number is invalid.");
        if ( show.getEpisodePerSeason() != null )
            hasAllFields[3] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show episode per season is invalid.");
        if ( show.getRating() > 1 && show.getRating() <= 10 )
            hasAllFields[4] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show rating is invalid.");
        if ( show.getYearOfRelease() > 1900 && show.getYearOfRelease() < 2023 )
            hasAllFields[5] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show year of release is invalid.");
        if ( show.getSeasons() > 0 && show.getSeasons() < 40 )
            hasAllFields[6] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show seasons is invalid.");
        if ( show.getCertificate() != null && show.getCertificate().length() < 10 )
            hasAllFields[7] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show certificate is invalid.");
        if ( show.getDirector() != null && show.getDirector().length() < 30 )
            hasAllFields[8] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show director is invalid.");
        if ( show.getGenre() != null && show.getGenre().length() < 15 )
            hasAllFields[9] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show genre is invalid.");
        if ( show.getWriter() != null && show.getWriter().length() < 30 )
            hasAllFields[10] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show writer is invalid.");
        if ( show.getPopularity() > 0 && show.getPopularity() < 1000 )
            hasAllFields[11] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show popularity is invalid.");
        if ( show.getPlot() != null && show.getPlot().length() < 1000 )
            hasAllFields[12] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show plot is invalid.");
        if ( show.getLanguage()!= null && show.getLanguage().length() < 15 )
            hasAllFields[13] = true;
        else
            throw new PayloadNotAcceptedException("Payload was not accepted because show language is invalid.");

        boolean finalState = true;

        for ( boolean state : hasAllFields )
            if ( !state )
                finalState = false;
        
        return finalState;
    }
    
    public boolean validateShowForUpdate(Show show) {
        if ( this.validateShow(show) && show.getShowId() != null )
            return true;
        
        return false;
    }
    
}
