package ShowServiceApplication.ShowService;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.hateoas.server.reactive.WebFluxLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class ShowModelAssembler implements RepresentationModelAssembler<Show, EntityModel<Show>> {
    
    @Override
    public EntityModel<Show> toModel(Show show) {
        return EntityModel.of(
            show,
            WebMvcLinkBuilder.linkTo(WebFluxLinkBuilder.methodOn(ShowController.class)
            .getShow(show.getShowId())).withSelfRel(),
            WebMvcLinkBuilder.linkTo(WebFluxLinkBuilder.methodOn(ShowController.class)
            .getShows(null, null, null, null, null, 0, 0, false, 0, 0, null)).withRel("shows")
        );
    }
    
}
