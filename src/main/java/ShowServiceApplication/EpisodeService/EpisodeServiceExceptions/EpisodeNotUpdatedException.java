package ShowServiceApplication.EpisodeService.EpisodeServiceExceptions;

public class EpisodeNotUpdatedException extends RuntimeException {
    public EpisodeNotUpdatedException() {
        super("Were not able to update episode.");
    }
}
