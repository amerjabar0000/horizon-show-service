package ShowServiceApplication.EpisodeService.EpisodeServiceExceptions;

public class EpisodeNotFoundException extends RuntimeException {
    public EpisodeNotFoundException() {
        super("Episode does not exist.");
    }
}
