package ShowServiceApplication.EpisodeService;

import ShowServiceApplication.ShowService.ShowServiceExceptions.PayloadNotAcceptedException;
import org.springframework.stereotype.Component;

@Component
public class EpisodeValidator {
    
    public boolean validateEpisode(Episode episode) {
        boolean[] hasAllFields = new boolean[9];
                
        if ( episode.getName() != null && episode.getName().length() < 50 )
            hasAllFields[0] = true;
        else
            throw new PayloadNotAcceptedException("Episode name is not valid");
        if ( episode.getDirector() != null && episode.getDirector().length() > 5 && episode.getDirector().length() < 50 )
            hasAllFields[1] = true;
        else
            throw new PayloadNotAcceptedException("Episode director is not valid");
        if ( episode.getPlot() != null && episode.getPlot().length() > 25 && episode.getPlot().length() < 500 )
            hasAllFields[2] = true;
        else
            throw new PayloadNotAcceptedException("Episode plot is not valid");
        if ( episode.getWriter() != null && episode.getWriter().length() > 5 && episode.getWriter().length() < 50 )
            hasAllFields[3] = true;
        else
            throw new PayloadNotAcceptedException("Episode writer is not valid");            
        if ( episode.getSeason() > 0 && episode.getSeason() < 50 )
            hasAllFields[4] = true;
        else
            throw new PayloadNotAcceptedException("Episode season is not valid");
        if ( episode.getRating() > 0 && episode.getRating() <= 10 )
            hasAllFields[5] = true;
        else
            throw new PayloadNotAcceptedException("Episode rating is not valid");
        if ( episode.getEpisodeLength() > 20 && episode.getEpisodeLength() <= 120 )
            hasAllFields[6] = true;
        else
            throw new PayloadNotAcceptedException("Episode length is not valid");
        if ( episode.getPopularity() > 0 && episode.getPopularity() <= 1000 )
            hasAllFields[7] = true;
        else
            throw new PayloadNotAcceptedException("Episode popularity is not valid");
        if ( episode.getEpisodeNumber() > 0 && episode.getEpisodeNumber() <= 50 )
            hasAllFields[8] = true;
        else
            throw new PayloadNotAcceptedException("Episode number is not valid");
        
        boolean finalState = true;
        
        for ( boolean state : hasAllFields )
            if ( !state )
                finalState = false;
        
        return finalState;
    }
    
}
