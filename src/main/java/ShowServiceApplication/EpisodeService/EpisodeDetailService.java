package ShowServiceApplication.EpisodeService;

import ShowServiceApplication.EpisodeService.EpisodeServiceExceptions.EpisodeNotFoundException;
import ShowServiceApplication.EpisodeService.EpisodeServiceExceptions.EpisodeNotUpdatedException;
import ShowServiceApplication.ShowService.Show;
import ShowServiceApplication.ShowService.ShowServiceExceptions.BadSearchQueryException;
import ShowServiceApplication.ShowService.ShowServiceExceptions.EpisodeNotCreatedException;
import ShowServiceApplication.ShowService.ShowServiceExceptions.PayloadNotAcceptedException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;

@Service
public class EpisodeDetailService {
    
    private final EpisodeRepository episodeRepository;
    private final EpisodeModelAssembler episodeAssembler;
    private final EpisodeValidator episodeValidator;
    
    public EpisodeDetailService (
        EpisodeRepository episodeRepository,
        EpisodeModelAssembler episodeAssembler,
        EpisodeValidator episodeValidator
    ) {
        this.episodeRepository = episodeRepository;
        this.episodeAssembler = episodeAssembler;
        this.episodeValidator = episodeValidator;
    }
    
    public List<EntityModel<Episode>> getEpisodes(
        String name,
        Double rating,
        Integer season,
        Integer page,
        Integer numberOfElements,
        String sortBy
    ) {
        List<EntityModel<Episode>> episodes;
        
        try {            
            if ( name != null ) {
                episodes = episodeRepository
                    .findByName(name, PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                    .stream()
                    .map(episode -> episode.get())
                    .map(episodeAssembler::toModel)
                    .collect(Collectors.toList());
            } else if ( rating != null ) {
                episodes = episodeRepository
                    .findByRating(rating, PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                    .stream()
                    .map(episode -> episode.get())
                    .map(episodeAssembler::toModel)
                    .collect(Collectors.toList());
            } else if ( season != null ) {
                episodes = episodeRepository
                    .findBySeason(season, PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                    .stream()
                    .map(episode -> episode.get())
                    .map(episodeAssembler::toModel)
                    .collect(Collectors.toList());                
            } else {
                episodes = episodeRepository
                    .findAllEpisodes(PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                    .stream()
                    .map(episodeAssembler::toModel)
                    .collect(Collectors.toList());
            }
            
        } catch ( Exception e ) {
            throw new BadSearchQueryException();
        }
        
        return episodes;
    }
    
    public EntityModel<Episode> getEpisode(UUID episodeId) {
        
        Optional<Episode> episode = episodeRepository.findById(episodeId);
        if ( !episode.isPresent() )
            throw new EpisodeNotFoundException();
        
        return EntityModel.of(episode.get());
    }
    
    public EntityModel<Episode> createEpisode(Show show, Episode newEpisode) {
        
        if ( !episodeValidator.validateEpisode(newEpisode) )
            throw new PayloadNotAcceptedException();
        
        if ( episodeRepository.findByNameAndShowId(newEpisode.getName(), show.getShowId()).isPresent() )
            throw new EpisodeNotCreatedException("Episode already exists in database.");
        
        Optional<Episode> episode;
        
        try {
            newEpisode.setShowReference(show);
            episodeRepository.save(newEpisode);
            episode = episodeRepository.findByNameAndShowId(newEpisode.getName(), show.getShowId());
        } catch ( Exception e ) {
            System.err.println(e);
            throw new EpisodeNotCreatedException("Could not create episode.");
        }
        
        return EntityModel.of(episode.get());
    }
    
    public EntityModel<Episode> updateEpisode(UUID episodeId, Episode episode) {
        
        if ( !episodeId.equals(episode.getEpisodeId()) )
            throw new PayloadNotAcceptedException("provided episode ids are not the same.");
        
        Optional<Episode> foundEpisode = episodeRepository.findById(episodeId);
        if ( !foundEpisode.isPresent() )
            throw new EpisodeNotFoundException();
        
        try {
            episode.setImages(foundEpisode.get().getImages());
            episode.setShowReference(foundEpisode.get().getShowReference());
            episodeRepository.save(episode);
        } catch ( Exception e ) {
            throw new EpisodeNotUpdatedException();
        }
        
        return EntityModel.of(episode);
    }
    
    public EntityModel<Episode> deleteEpisode(UUID episodeId) {
        
        Optional<Episode> foundEpisode = episodeRepository.findById(episodeId);
        if ( !foundEpisode.isPresent() )
            throw new EpisodeNotFoundException();
        
        try {
            episodeRepository.deleteById(episodeId);
        } catch ( Exception e ) {
            throw new EpisodeNotFoundException();
        }
        
        return EntityModel.of(foundEpisode.get());
    }
    
    public void deleteEpisodes(UUID showId) {
        
        try {
            episodeRepository.deleteAllEpisodesByShowId(showId);
        } catch ( Exception e ) {
            System.err.println(e);
            throw new EpisodeNotFoundException();
        }
        
    }
    
}

