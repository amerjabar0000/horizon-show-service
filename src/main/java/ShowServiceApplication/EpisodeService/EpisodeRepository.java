package ShowServiceApplication.EpisodeService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface EpisodeRepository extends JpaRepository<Episode, UUID> {
    
    @Query(
        nativeQuery = true,
        value = "SELECT * FROM episodes"
    )
    List<Episode> findAllEpisodes(Pageable page);
    
    @Query(
        nativeQuery = true,
        value = "SELECT * FROM episodes WHERE name ILIKE %?%"
    )
    List<Optional<Episode>> findByName(String name, Pageable page);
    
    @Query(
        nativeQuery = true,
        value = "SELECT * FROM episodes WHERE rating > ?"
    )
    List<Optional<Episode>> findByRating(double rating, Pageable page);
    
    @Query(
        nativeQuery = true,
        value = "SELECT * FROM episodes WHERE season = ?"
    )
    List<Optional<Episode>> findBySeason(int season, Pageable page);
    
    @Query(
        nativeQuery = true,
        value = "DELETE FROM episodes WHERE show_id_reference = ?"
    )
    @Transactional
    @Modifying
    void deleteAllEpisodesByShowId(UUID showId);
    
    @Query(
        nativeQuery = true,
        value = "SELECT * FROM episodes WHERE name = ? AND show_id_reference = ?"
    )
    Optional<Episode> findByNameAndShowId(String name, UUID showId);
    
}

