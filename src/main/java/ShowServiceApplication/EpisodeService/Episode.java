package ShowServiceApplication.EpisodeService;

import ShowServiceApplication.EpisodeImageService.EpisodeImage;
import ShowServiceApplication.ShowService.Show;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name = "Episodes")
@Table(name = "episodes")
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Episode implements Serializable {
    
    @Column(name = "episode_id", unique = true, nullable = false)
    private @Id @GeneratedValue UUID episodeId;
    
    @Column(name = "episode_number", nullable = false)
    private int episodeNumber;
    
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "director", nullable = false)
    private String director;

    @Column(name = "writer", nullable = false)
    private String writer;

    @Column(name = "plot", nullable = false, columnDefinition = "TEXT", length = 1000)
    private String plot;
    
    @Column(name = "season", nullable = false)
    private int season;

    @Column(name = "rating", nullable = false)
    private double rating;

    @Column(name = "episode_length", nullable = false)
    private int episodeLength;

    @Column(name = "popularity", nullable = false)
    private int popularity;
    
    @OneToMany(mappedBy = "episodeReference", targetEntity = EpisodeImage.class, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<EpisodeImage> images;
    
    @ManyToOne
    @JoinColumn(name = "show_id_reference", referencedColumnName = "show_id")
    @JsonBackReference
    private Show showReference;
    
    public Episode() {}
    
    public Episode(UUID episodeId, int episodeNumber, String name, String director, String writer, String plot, int season, double rating, int episodeLength, int popularity, List<EpisodeImage> images) {
        this.episodeId = episodeId;
        this.episodeNumber = episodeNumber;
        this.name = name;
        this.director = director;
        this.writer = writer;
        this.plot = plot;
        this.season = season;
        this.rating = rating;
        this.episodeLength = episodeLength;
        this.popularity = popularity;
        this.images = images;
    }
    
    public void deleteEpisodeImage(UUID id) {
        for ( int x = 0; x < this.images.size(); x++ ) {
            if ( this.images.get(x).getEpisodeImageId().equals(id) )
                this.images.remove(x);
        }
    }
    
    public void deleteEpisodeImages() {
        while ( !this.images.isEmpty() )
            this.images.remove(0);
    }

    public void setEpisodeId(UUID episodeId) {
        this.episodeId = episodeId;
    }
    
    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setEpisodeLength(int episodeLength) {
        this.episodeLength = episodeLength;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public void setImages(List<EpisodeImage> images) {
        this.images = images;
    }

    public void setShowReference(Show showReference) {
        this.showReference = showReference;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }
    
    public Show getShowReference() {
        return showReference;
    }
    
    public UUID getEpisodeId() {
        return episodeId;
    }

    public String getName() {
        return name;
    }

    public String getDirector() {
        return director;
    }

    public String getWriter() {
        return writer;
    }

    public String getPlot() {
        return plot;
    }

    public int getSeason() {
        return season;
    }

    public double getRating() {
        return rating;
    }

    public int getEpisodeLength() {
        return episodeLength;
    }

    public int getPopularity() {
        return popularity;
    }

    public List<EpisodeImage> getImages() {
        return images;
    }
    
}
