package ShowServiceApplication.EpisodeService.EpisodeServiceResponses;

import ShowServiceApplication.EpisodeService.EpisodeServiceExceptions.EpisodeNotUpdatedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class EpisodeNotUpdatedResponse {
    
    @ResponseBody
    @ExceptionHandler(EpisodeNotUpdatedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String episodeNotUpdatedHandler(EpisodeNotUpdatedException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
