package ShowServiceApplication.EpisodeService.EpisodeServiceResponses;

import ShowServiceApplication.EpisodeService.EpisodeServiceExceptions.EpisodeNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class EpisodeNotFoundResponse {
    
    @ResponseBody
    @ExceptionHandler(EpisodeNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String episodeNotFoundHandler(EpisodeNotFoundException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
