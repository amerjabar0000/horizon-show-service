package ShowServiceApplication.EpisodeService;

import ShowServiceApplication.ShowService.ShowController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.hateoas.server.reactive.WebFluxLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class EpisodeModelAssembler implements RepresentationModelAssembler<Episode, EntityModel<Episode>> {
    
    @Override
    public EntityModel<Episode> toModel(Episode episode) {
        return EntityModel.of(
            episode,
            WebMvcLinkBuilder.linkTo(WebFluxLinkBuilder.methodOn(ShowController.class)
            .getEpisode(episode.getShowReference().getShowId(), episode.getEpisodeId())).withSelfRel(),
            WebMvcLinkBuilder.linkTo(WebFluxLinkBuilder.methodOn(ShowController.class)
            .getEpisodes(episode.getShowReference().getName(), null, 0, 0, 0, null)).withRel("episodes")
        );
    }
    
}
