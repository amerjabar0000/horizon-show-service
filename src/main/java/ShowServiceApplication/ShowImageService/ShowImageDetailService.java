package ShowServiceApplication.ShowImageService;

import ShowServiceApplication.ImageServiceExceptions.ImageNotCreatedException;
import ShowServiceApplication.ImageServiceExceptions.ImageNotFoundException;
import ShowServiceApplication.ShowService.Show;
import ShowServiceApplication.ShowService.ShowServiceExceptions.PayloadNotAcceptedException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ShowImageDetailService {
    
    private final ShowImageRepository showImageRepository;
    private final ShowImageValidator showImageValidator;
    
    public ShowImageDetailService(
        ShowImageRepository showImageRepository,
        ShowImageValidator showImageValidator
    ) {
        this.showImageRepository = showImageRepository;
        this.showImageValidator = showImageValidator;
    }
    
    public ShowImage createShowImage(ShowImage image) {
        
        if ( !showImageValidator.validateShowImage(image) )
            throw new PayloadNotAcceptedException();
            
        try {
            showImageRepository.save(image);
        } catch ( Exception e ) {
            throw new ImageNotCreatedException();
        }
        
        return image;
    }
    
    public Optional<ShowImage> getShowImage(UUID id) {
        
        Optional<ShowImage> image = showImageRepository.findByUUID(id);        
        if ( !image.isPresent() )
            throw new ImageNotFoundException();
        
        return image;
    }

    public List<ShowImage> createShowImages(List<ShowImage> images, Show show) {
        
        if ( !showImageValidator.validateShowImages(images) )
            throw new PayloadNotAcceptedException();
        
        List<ShowImage> newImages = new ArrayList<>();
        
        try {
            for ( int x = 0; x < images.size(); x++ ) {                
                images.get(x).setShowReference(show);
                newImages.add(showImageRepository.save(images.get(x)));
            }
        } catch ( Exception e ) {
            throw new ImageNotCreatedException();
        }
        
        return newImages;
    }
    
    public List<ShowImage> getImagesOfShow(UUID id, int page, int numberOfElements, String sortBy) {
        
        List<ShowImage> images;
        
        try {
            images = showImageRepository
                .findShowImagesByShowId(id, PageRequest.of(page, numberOfElements, Sort.by(sortBy)))
                .stream()
                .collect(Collectors.toList());
        } catch ( Exception e ) {
            System.err.println(e);
            throw new ImageNotFoundException();
        }
       
        return images;
    }
    
    public void deleteShowImage(ShowImage image) {
        try {
            showImageRepository.delete(image);
        } catch ( NoSuchElementException | EmptyResultDataAccessException e ) {
            throw new ImageNotFoundException();
        }
    }
    
    public void deleteAllShowImages(UUID showId) {
        try {
            showImageRepository.deleteAllImagesOfShow(showId);
        } catch ( NoSuchElementException | EmptyResultDataAccessException e ) {
            throw new ImageNotFoundException();
        }
    }
    
}


