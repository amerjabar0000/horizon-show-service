package ShowServiceApplication.ShowImageService;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ShowImageRepository extends JpaRepository<ShowImage, Long> {
    
    @Query(
        value = "SELECT * FROM show_images WHERE show_image_id = ?",
        nativeQuery = true
    )
    Optional<ShowImage> findByUUID(UUID id);
    
    @Query(
        value = "SELECT * FROM show_images WHERE show_id_reference = ?",
        nativeQuery = true
    )
    Page<ShowImage> findShowImagesByShowId(UUID id, Pageable page);
    
    @Modifying
    @Transactional
    @Query(
        value = "DELETE FROM show_images WHERE show_id_reference = ?",
        nativeQuery = true
    )
    void deleteAllImagesOfShow(UUID showId);
    
}

