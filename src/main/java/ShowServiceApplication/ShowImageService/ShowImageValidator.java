package ShowServiceApplication.ShowImageService;

import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class ShowImageValidator {
    
    private final ShowImageRepository showImageRepository;
    
    public ShowImageValidator(ShowImageRepository showImageRepository) {
        this.showImageRepository = showImageRepository;
    }
    
    public boolean validateShowImage(ShowImage image) {
        if ( image.getDateOfAdding() != null )
            return true;
        return false;
    }
    
    public boolean showImageExists(ShowImage image) {
        return showImageRepository.findByUUID(image.getShowImageId()).isPresent();
    }
    
    public boolean showImageExists(List<ShowImage> images) {
        for ( ShowImage image : images ) {
            if ( !showImageExists(image) )
                return false;
        }
        
        return true;
    }
    
    public boolean validateShowImages(List<ShowImage> images) {
        
        if ( images.size() < 1 )
            return false;
        
        boolean finalState = true;
        
        for ( int x = 0; x < images.size(); x++ ) {
            if ( !validateShowImage(images.get(x)) )
                finalState = false;
        }
        
        return finalState;
    }
    
}
