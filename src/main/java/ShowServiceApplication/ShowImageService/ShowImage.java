package ShowServiceApplication.ShowImageService;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import ShowServiceApplication.ShowService.Show;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.io.Serializable;
import java.util.UUID;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.ColumnDefault;

@Entity(name = "ShowImages")
@Table(name = "show_images")
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ShowImage implements Serializable {
    
    @Column(name = "show_image_id", unique = true, nullable = false)
    private @Id @GeneratedValue UUID showImageId;
    
    @ManyToOne
    @JoinColumn(name = "show_id_reference", referencedColumnName = "show_id")
    @JsonBackReference
    private Show showReference;

    @Column(name = "date_of_adding", unique = false, nullable = true, columnDefinition = "TEXT")
    private String dateOfAdding;
    
    @Column(name = "show_image_priority", nullable = true, columnDefinition = "varchar(10) defaul 'none'")
    private String showImagePriority;
    
    public ShowImage() {};
    
    public ShowImage(Show showReference, String dateOfAdding, String showImagePriority) {
        this.showReference = showReference;
        this.dateOfAdding = dateOfAdding;
        this.showImagePriority = showImagePriority;
    }

    public void setShowImageId(UUID showImageId) {
        this.showImageId = showImageId;
    }

    public String getShowImagePriority() {
        return showImagePriority;
    }

    public void setShowImagePriority(String showImagePriority) {
        this.showImagePriority = showImagePriority;
    }

    public void setShowReference(Show showReference) {
        this.showReference = showReference;
    }

    public void setDateOfAdding(String dateOfAdding) {
        this.dateOfAdding = dateOfAdding;
    }

    public UUID getShowImageId() {
        return showImageId;
    }

    public Show getShowReference() {
        return showReference;
    }

    public String getDateOfAdding() {
        return dateOfAdding;
    }
    
}
