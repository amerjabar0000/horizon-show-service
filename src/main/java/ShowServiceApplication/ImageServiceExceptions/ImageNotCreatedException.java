package ShowServiceApplication.ImageServiceExceptions;

import org.springframework.stereotype.Component;

@Component
public class ImageNotCreatedException extends RuntimeException {
    public ImageNotCreatedException() {
        super("Were not able to create uploaded image.");
    }
}
