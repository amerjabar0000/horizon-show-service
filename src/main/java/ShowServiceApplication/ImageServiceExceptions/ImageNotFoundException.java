package ShowServiceApplication.ImageServiceExceptions;

import org.springframework.stereotype.Component;

@Component
public class ImageNotFoundException extends RuntimeException {
    public ImageNotFoundException() {
        super("Image does not exist.");
    }
    
    public ImageNotFoundException(String message) {
        super(message);
    }
}
