package ShowServiceApplication.ImageServiceExceptions;

import org.springframework.stereotype.Component;

@Component
public class ImageNotUpdatedException extends RuntimeException {
    public ImageNotUpdatedException() {
        super("Were not able to update the image.");
    }
}

