package ShowServiceApplication.ImageServiceResponses;

import ShowServiceApplication.ImageServiceExceptions.ImageNotCreatedException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Component
@ControllerAdvice
public class ImageNotCreatedResponse {
    
    @ResponseBody
    @ExceptionHandler(ImageNotCreatedException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String imageNotCreatedHandler(ImageNotCreatedException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
