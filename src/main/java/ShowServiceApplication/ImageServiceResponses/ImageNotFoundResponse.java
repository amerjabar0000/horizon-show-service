package ShowServiceApplication.ImageServiceResponses;

import ShowServiceApplication.ImageServiceExceptions.ImageNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Component
@ControllerAdvice
public class ImageNotFoundResponse {
    
    @ResponseBody
    @ExceptionHandler(ImageNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String imageNotFoundHandler(ImageNotFoundException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
