package ShowServiceApplication.ImageServiceResponses;

import ShowServiceApplication.ImageServiceExceptions.ImageNotUpdatedException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Component
@ControllerAdvice
public class ImageNotUpdatedResponse {
    
    @ResponseBody
    @ExceptionHandler(ImageNotUpdatedException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String imageNotUpdatedHandler(ImageNotUpdatedException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
