FROM openjdk:8-jdk-alpine
ENV APP_HOME=/usr/app
ENV PORT 8080
EXPOSE 8080
WORKDIR $APP_HOME
COPY target/*.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]